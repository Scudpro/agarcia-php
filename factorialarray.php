<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>FactorialArray</title>
    </head>
    <body>
        <?php
        $num = array(1, 15, 13, 23);
        $longi = count($num);
        $aux = true;
        $numfinal = array();

        for ($i = 0; $i < $longi; $i++) {
            if (is_array($num) && is_int($num[$i])) {
                $numfinal[$i] = factorial($num[$i]);
                echo " Factorial:" . $numfinal[$i] . "<br>";
            } else {
                echo "Hi ha algun valor que no és un valor vàlid";
                $aux = false;
            }
        }

        function factorial($num) {
            if ($num == 1) {
                return 1;
            } else {
                return ($num) * factorial($num - 1);
            }
        }

        print_r($numfinal);
        ?>
    </body>
</html>
