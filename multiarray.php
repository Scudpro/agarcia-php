<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Multiarray</title>
    </head>
    <body>
        <?php

        function CrearTaula($valor) {
            $arraytaula = array();
            for ($i = 0; $i < $valor; $i++) {
                for ($j = 0; $j < $valor; $j++) {
                    if ($i == $j) {
                        $arraytaula[$i][$j]= "*" ;
                    } elseif ($i < $j) {
                        $arraytaula[$i][$j]=($i + $j);
                    } else {
                        $arraytaula[$i][$j]= rand(10, 20);
                    }
                }
            }
            MostrarTaula($arraytaula);
            GirarTaula($arraytaula);
        }
        
        
        
        function MostrarTaula($arraytaula) {
            echo "<table border='1'>";
            for ($i = 0; $i < count($arraytaula); $i++) {
                echo "<tr>";
                for ($j = 0; $j < count($arraytaula); $j++) {
                    echo "<td> ".$arraytaula[$i][$j]. "</td>";
                }
                echo "</tr>";
            }
            echo "</table>";
        }
        


        function GirarTaula($arraytaula) {
            $girada = array();
            foreach ($arraytaula as $files => $columns) {
                foreach ($columns as $files2 => $column2) {
                    $girada[$files2][$files] = $column2;
                }
            }
            Mostrartaula($girada);
        }

        CrearTaula(4);
        ?>
    </body>
</html>
